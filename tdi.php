<!DOCTYPE html>
<html>
<?php
    session_start();
    $nikk = $_SESSION['nik'];
    $status = $_SESSION['status'];
    if ($status != true){
        header("location:index.php?pesan=belum_login");
    }

?>
<head>
	<title>Detail Insiden Baru</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link type="text/css" href="jquery-ui-1.10.3/themes/base/jquery.ui.all.css" rel="stylesheet"/>
    <script type="text/javascript" src="jquery-ui-1.10.3/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/jquery.ui.core.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/jquery.ui.progressbar.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/jquery.ui.position.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/jquery.ui.resizable.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/jquery.ui.mouse.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/jquery.ui.button.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/jquery.ui.dialog.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/jquery.ui.effect.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/i18n/jquery.ui.datepicker-id.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/jquery.ui.menu.js"></script>
    <script type="text/javascript" src="jquery-ui-1.10.3/ui/jquery.ui.autocomplete.js"></script>
</head>
<body>

<!--navnye-->
<nav class="navbar navbar-expand-sm bg-danger navbar-dark"  >
  <!-- Brand/logo -->
  <a class="navbar-brand" href="home.php">
    <img src="gambar/kisel.png" alt="logo" style="width: 100px; border-radius: 10px; border: 3px solid #ffffff; background-color: #ffffff;">
  </a>

  <!-- Links -->
  <ul class="navbar-nav" style=" margin-left: 375px; ">
    <li class="nav-item" style="border: 0px solid; width: 350px; border-radius: 10px;">
      <a class="nav-link" href="home.php" role="button" aria-haspopup="true" aria-expanded="false" 
      style="color: #ffffff; text-align: center; font-size: 26px;">Incident Report Security</a>
    </li>
    <li class="nav-item dropdown" style="margin-left: 450px; border-radius: 10px;">
      <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <img src="gambar/id.png" style="width: 35px;"></img>
        </a>
      <div class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item" href="gantipw.php">Ganti Password</a>
        <a class="dropdown-item" href="logout.php">Logout</a>
      </div>
    </li>
  </ul>
</nav>
<!--navnye-->

<br>
<br>
<div class="container" style="border-radius: 10px; background-color: #DCDCDC; height: 550px; width: 690px;">
  <br>
  <h3 style="text-align: center; font-family: Helvetica;">Data Insiden Baru</h3>
  <br>
        <?php
        include 'koneksi.php';
        $id = $_GET['id'];
        $qry = mysqli_query($koneksi,"select * from insiden where NIk = '$id'");
        $row = mysqli_fetch_array($qry);
        $KYD = $_POST['KYD'];
        $KYD = $row['KYD'];
        $DI = $_POST['DI'];
        $DI = $row['durasi'];
        $WK = $_POST['WK'];
        $WK= $row['WK'];
        $dampak = $_POST['dampak'];
        $dampak = $row['dampak'];
        $penyebab = $_POST['penyebab'];
        $penyebab = $row['penyebab'];
        $DK = $_POST['DK'];
        $DK = $row['DK'];
        $penang = $_POST['penanggulangan'];
        $penang = $row['penanggulangan'];
        $preventif = $_POST['preventif'];
        $preventif = $row['preventif'];
        $id = $id;
        ?>
  <form action="#" method="post">
      <input type=hidden name=nik value=<?php echo $nikk ?>>
    <div>
      <label style="width: 275px;">Kejadian Yang Dialami</label>
      <input type="text" class="inline" name="KYD" value="<?php echo $KYD;?> style="width: 375px; padding: 5px 5px; border-radius: 5px; margin-bottom: 10px;" required>
    </div>
    <div>
      <label style="width: 275px;">Durasi Insiden</label>
      <input type="text" class="inline" name="DI" style="width: 375px; padding: 5px 5px; border-radius: 5px; margin-bottom: 10px;"  value="<?php echo $DI;?>>
    </div>
    <div>
      <label style="width: 275px;">Waktu Kejadian</label>
      <input type="text" class="inline" name="WK" style="width: 375px; padding: 5px 5px; border-radius: 5px; margin-bottom: 10px;" value="<?php echo $WK;?>>
    </div>
    <div>
      <label style="width: 275px;">Dampak</label>
      <input type="text" class="inline" name="dampak" style="width: 375px; padding: 5px 5px; border-radius: 5px; margin-bottom: 10px;" value="<?php echo $dampak;?>>
    </div>
    <div>
      <label style="width: 275px;">Penyebab</label>
      <input type="text" class="inline" name="penyebab" style="width: 375px; padding: 5px 5px; border-radius: 5px; margin-bottom: 10px;" value="<?php echo $penyebab;?>>
    </div>
    <div>
      <label style="width: 275px;">Detail Kejadian</label>
      <input type="text" class="inline" name="DK" style="width: 375px; padding: 5px 5px; border-radius: 5px; margin-bottom: 10px;" value="<?php echo $DK;?>>
    </div>
    <div>
      <label style="width: 275px;">Penanggulangan</label>
      <input type="text" class="inline" name="penanggulangan" style="width: 375px; padding: 5px 5px; border-radius: 5px; margin-bottom: 10px;" value="<?php echo $penang;?>>
    </div>
    <div>
      <label style="width: 275px;">Tindakan Preventif</label>
      <input type="text" class="inline" name="preventif" value="<?php echo $preventif;?> style="width: 375px; padding: 5px 5px; border-radius: 5px; margin-bottom: 10px;">
    </div>
    <button type='submit' name='submit' class="btn" style="padding: 5px 5px; border-radius: 10px; background-color: #049372; cursor: pointer; color: white; width: 120px; margin-left: 535px; margin-top: 10px;">Simpan</button>
  </form>

</div>

<br>
<br>
<br>

</body>
</html>
