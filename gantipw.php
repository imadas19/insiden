<!DOCTYPE html>
<html>
<head>
    <title>Ganti Password</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/gantipw.css" type="text/css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>

<!--navnye-->
<nav class="navbar navbar-expand-sm bg-danger navbar-dark"  >
  <!-- Brand/logo -->
  <a class="navbar-brand" href="home.php">
    <img src="gambar/logo1.png" alt="logo" style="width: 100px; border-radius: 10px; border: 3px solid #ffffff;">
  </a>

  <!-- Links -->
  <ul class="navbar-nav" style="margin-left: 180px; padding-bottom: -19px;">
    <li class="nav-item dropdown" style="border: 0px solid; width: 250px; border-radius: 10px;">
      <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: #ffffff; text-align: center;">Registrasi Mitra</a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="rm/tmb.php">Tambah Mitra Baru</a>
        <a class="dropdown-item" href="rm/ddm.php">Daftar Data Mitra</a>
        <a class="dropdown-item" href="rm/drm.php">Daftar Registrasi Mitra</a>
        <a class="dropdown-item" href="rm/tbu.php">Tambah Bidang Usaha</a>
      </div>
    </li>
    <li class="nav-item dropdown" style="border:0px solid 049372; width: 250px; margin: 0 10px; border-radius: 10px;">
      <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: #ffffff; text-align: center;">Kinerja Mitra</a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="km/rm.php">Tambah Kinerja Mitra</a>
        <a class="dropdown-item" href="km/rmpm.php">Laporan Kinerja Per Mitra</a>
        <a class="dropdown-item" href="km/rmpb.php">Laporan Kinerja Per Bulan</a>
      </div>
    </li>
    <li class="nav-item dropdown" style="border: 0px solid; width: 250px; border-radius: 10px;">
      <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: #ffffff; text-align: center;">Rating Kisel</a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="rk/lrpm.php">Laporan Rating Kisel Permitra</a>
        <a class="dropdown-item" href="rk/lrpb.php">Laporan Kinerja Mitra Perbulan</a>
      </div>
    </li>
    <li class="nav-item dropdown" style="margin-left: 215px; border-radius: 10px;">
      <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <img src="gambar/id.png" style="width: 35px;"></img>
        </a>
      <div class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item" href="gantipw.php">Ganti Password</a>
        <a class="dropdown-item" href="logout.php">Logout</a>
      </div>
    </li>
  </ul>
</nav>
<!--navnye-->

<br>
<br>
<form action="change.php" method="post">
  <div class="container">
    <h3 style="text-align: center; font-family: Helvetica;">Ganti Password</h3>
    <br>
    <div>
      <label style="width: 210px;">Password Lama</label>
      <input type="password" placeholder="" name="password" class="inline" required>
    </div>
    <div>
      <label style="width: 210px;">Password Baru</label>
      <input type="password" placeholder="" name="password" class="inline" required>
    </div>
    <div>
      <label style="width: 210px;">Konfirmasi Password Baru</label>
      <input type="password" placeholder="" name="password" class="inline" required>
    </div>
    <button type="submit" class="ganti">Ganti</button>
</form>

</body>
</html>
