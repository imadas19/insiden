<!DOCTYPE html>
<html>
<head>
	<title>Login Incident Report</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/login.css" type="text/css">
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>

<br>
<br>
<br>
<br>
<br>
<div class="imgcontainer">
  <img align="center" src="gambar/kisel.png" alt="Avatar" class="avatar">
</div>
  
<br>
<br>
<form action="cek_login.php" method="post">
  <?php
  if(isset($_GET['pesan'])){
    if($_GET['pesan'] == "gagal"){
      echo "<script type='text/javascript'>
        alert('Username & Password Anda Salah!');
        history.back(self);
        </script>";
    }else if($_GET['pesan'] == "logout"){
      echo "<script type='text/javascript'>
        alert('Anda Berhasil Logout');
        window.location('index.php');
        </script>";
    }else if ($_GET['pesan'] == "belum_login"){
        echo "<script type='text/javascript'>
        alert('silahkan login terlebih dahulu');
        window.location('index.php');
        </script>";
    }
  }
  ?>
  <div class="container">
    <label style="width: 90px;"><b>Username</b></label>
    <input class="inline" type="text" placeholder="Enter Username" name="nama" required>
    <label style="width: 90px;"><b>Password</b></label>
    <input class="inline" type="password" placeholder="Enter Password" name="pass" required>
    <button type="submit" class="masuk">Masuk</button>
  </div>
  <div>  
    <span class="bpa"><a href="lupepw.php" style="color: #0000ff;">Lupa Kata Sandi?</a></span>
  </div>
</form>
</body>
</html>
